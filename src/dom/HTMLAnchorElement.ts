import { HTMLElement } from './index'

export class HTMLAnchorElement extends HTMLElement implements globalThis.HTMLAnchorElement {
  download
  hash
  host
  hostname
  href
  hreflang
  origin
  password
  pathname
  ping
  port
  protocol
  referrerPolicy
  rel
  relList
  search
  target
  text
  type
  username

  // deprecated
  charset
  coords  
  name
  rev
  shape
}
