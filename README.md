# graffiti

HTML/CSS engine for node.js and deno.

Currently, it is just my hobby/research project and it's **not yet intended for any use**.

## Dev

```
npm install
npm run build
npm run prepare

# and then
deno run -A --unstable examples/hello.js

# or
node examples/hello.js
```

![react-calculator](https://github.com/cztomsik/graffiti/blob/e7035110f6c6e38fa957871c6df80741690a70b1/docs/images/react-calculator.png?raw=true)

![hmr](https://github.com/cztomsik/graffiti/blob/e7035110f6c6e38fa957871c6df80741690a70b1/docs/images/hmr.gif?raw=true)

![hackable-tv](https://user-images.githubusercontent.com/3526922/74057963-4ad47f00-49e5-11ea-9e0d-b39c98f5fe1b.gif)

## Contributing

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as below, without any additional terms or conditions.

## License

Copyright 2018 Kamil Tomšík [info@tomsik.cz]

This project is licensed under either of

- [Apache License, Version 2.0](https://www.apache.org/licenses/LICENSE-2.0) ([`LICENSE-APACHE`](LICENSE-APACHE))
- [MIT license](https://opensource.org/licenses/MIT) ([`LICENSE-MIT`](LICENSE-MIT))

at your option.

The [SPDX](https://spdx.dev) license identifier for this project is `MIT OR Apache-2.0`.
